from pytest import mark
from src.palindrome import Solution


class TestSuit:

    @mark.test_case
    def test_non_palindromes(self):
        actual = Solution()

        assert actual.solution(
            "arma") is False, "'arma' it's not a palindrome"
        assert actual.solution(
            "Marica") is False, "'Marica' it's not a palindrome"
        assert actual.solution(
            "") is False, "An empty string it's not a palindrome"
        assert actual.solution(
            "Python") is False, "'Python' it's not a palindrome"
        assert actual.solution(
            "Arepa") is False, "'Arepa' it's not a palindrome"
        assert actual.solution(
            "Hello World") is False, "'Hello World' it's not a palindrome"
        assert actual.solution(
            "Burrita") is False, "'Burrita' it's not a palindrome"

    @mark.test_case
    def test_palindromes(self):
        actual = Solution()

        assert actual.solution(
            "La ruta natural") is True, "'La ruta natural' it's supposed to be a palindrome"
        assert actual.solution(
            "Ababa") is True, "'Ababa' it's supposed to be a palindrome"
        assert actual.solution(
            "seres") is True, "'seres' it's supposed to be a palindrome"
        assert actual.solution(
            "solos") is True, "'solos' it's supposed to be a palindrome"
        assert actual.solution(
            "RADAR") is True,  "'RADAR' it's supposed to be a palindrome"
        assert actual.solution(
            "amor a roma") is True, "'amor a roma' it's supposed to be a palindrome"
        assert actual.solution(
            "oJo") is True, "'oJo' it's supposed to be a palindrome"
